package seleniumtest;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.*;

import java.time.Duration;

public class TestSelenium {

    WebDriver driver;
    @BeforeClass
    public  void  openBrowser(){
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(Duration.ofMillis(10000));
    }

    @AfterMethod
    public void logout() throws InterruptedException {
        driver.navigate().to("https://katalon-demo-cura.herokuapp.com/authenticate.php?logout");
        Thread.sleep(2000);
    }

    @Test
    public void TC_01(){
        driver.navigate().to("https://katalon-demo-cura.herokuapp.com/profile.php#login");

        String username = driver.findElement(By.xpath("/html/body/section/div/div/div[2]/form/div[1]/div[1]/div/div/input")).getAttribute("value");
        String password = driver.findElement(By.xpath("/html/body/section/div/div/div[2]/form/div[1]/div[2]/div/div/input")).getAttribute("value");

        driver.findElement(By.id("txt-username")).sendKeys(username);
        driver.findElement(By.id("txt-password")).sendKeys(password);

        driver.findElement(By.id("btn-login")).click();

        String actualResult = driver.findElement(By.id("btn-book-appointment")).getText();
        String expected = "Book Appointment";
        Assert.assertEquals(actualResult,expected);

    }

    @Test
    public void TC_02(){
        driver.navigate().to("https://katalon-demo-cura.herokuapp.com/profile.php#login");

        String username = "John Doe";
        String password = "password salah";

        driver.findElement(By.id("txt-username")).sendKeys(username);
        driver.findElement(By.id("txt-password")).sendKeys(password);

        driver.findElement(By.id("btn-login")).click();

        String actualResult = driver.findElement(By.className("text-danger")).getText();
        String expected = "Login failed! Please ensure the username and password are valid.";
        Assert.assertEquals(actualResult,expected);
    }

    @Test
    public void TC_03(){
        driver.navigate().to("https://katalon-demo-cura.herokuapp.com/profile.php#login");

        String username = "username palsu";
        String password = "ThisIsNotAPassword";

        driver.findElement(By.id("txt-username")).sendKeys(username);
        driver.findElement(By.id("txt-password")).sendKeys(password);

        driver.findElement(By.id("btn-login")).click();

        String actualResult = driver.findElement(By.className("text-danger")).getText();
        String expected = "Login failed! Please ensure the username and password are valid.";
        Assert.assertEquals(actualResult,expected);
    }

    @Test
    public void TC_04(){
        driver.navigate().to("https://katalon-demo-cura.herokuapp.com/profile.php#login");

        String username = "username palsu";
        String password = "password salah";

        driver.findElement(By.id("txt-username")).sendKeys(username);
        driver.findElement(By.id("txt-password")).sendKeys(password);

        driver.findElement(By.id("btn-login")).click();

        String actualResult = driver.findElement(By.className("text-danger")).getText();
        String expected = "Login failed! Please ensure the username and password are valid.";
        Assert.assertEquals(actualResult,expected);
    }


    @DataProvider(name = "data-login")
    public Object[][] dataLogin(){
        return new Object[][]{
                {"John Doe", "ThisIsNotAPassword", "success"},
                {"username palsu", "ThisIsNotAPassword", "failed"},
                {"John Doe", "password salah", "failed"},
                {"username palsu", "password salah", "failed"},
        };
    }

    @Test(dataProvider = "data-login")
    public void TC_05(String username, String password, String status){
        driver.navigate().to("https://katalon-demo-cura.herokuapp.com/profile.php#login");

        driver.findElement(By.id("txt-username")).sendKeys(username);
        driver.findElement(By.id("txt-password")).sendKeys(password);

        driver.findElement(By.id("btn-login")).click();

        if(status.equals("success")){
            String actualResult = driver.findElement(By.id("btn-book-appointment")).getText();
            String expected = "Book Appointment";
            Assert.assertEquals(actualResult,expected);
        }

        if(status.equals("failed")){
            String actualResult = driver.findElement(By.className("text-danger")).getText();
            String expected = "Login failed! Please ensure the username and password are valid.";
            Assert.assertEquals(actualResult,expected);
        }

    }

    @AfterClass
    public  void closeBrowser() throws InterruptedException {
        Thread.sleep(5000);
        driver.quit();

    }
}
